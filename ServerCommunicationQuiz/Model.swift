//
//  Model.swift
//  ServerCommunicationQuiz
//
//  Created by BTB_015 on 12/9/20.
//

import Foundation


struct Person: Codable {
    
    let first_name: String
    let last_name: String
    let email: String
    let avatar: String
    let id: Int
    
}

struct GetData: Codable{
    var data: [Person]
}
