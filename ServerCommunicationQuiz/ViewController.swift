//
//  ViewController.swift
//  ServerCommunicationQuiz
//
//  Created by BTB_015 on 12/9/20.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    var person: [Person] = []
    var data: Data?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let url = URL(string: "https://reqres.in/api/users?page=2")
        let reqUrl = URLRequest(url:url!)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        
        let task = session.dataTask(with: reqUrl) { (data, response, error) in
            
            if error != nil && data == nil{
                          return
            }

            guard let data = data else{
                print(error.debugDescription)
                return
            }
            
            do {
                let response = try JSONDecoder().decode(GetData.self, from: data)
                for rec in response.data {
                    print(rec.first_name)
                    self.person.append(contentsOf: [rec])

                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            } catch let error {
                print(error)
            }
        }
 
        task.resume()
    }
}


extension ViewController: UITabBarDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return person.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.detailTextLabel?.text = "mm"
        cell.textLabel?.text = "\(person[indexPath.row].first_name ) \(person[indexPath.row].last_name )"
        cell.detailTextLabel?.text = person[indexPath.row].last_name
               
        let url = URL(string: "\(person[indexPath.row].avatar)")
        let data = try? Data(contentsOf: url!)
        let myImage = UIImage(data: data!)
        cell.imageView?.image = myImage

        return cell
    }
    
    
}
